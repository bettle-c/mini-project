package io.shentuzhigang.exam.zhiyitech;

import java.util.Arrays;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-07 18:41
 */
public class ArrayReduce {

    /**
     * 使用Arrays类中的sort方法实现排序，然后用后面的值与前面的一个比较，重复则加入到新数组之中
     * @param arr 待去重数组
     * @return 升序的去重的数组
     */
    public static int[] sortToReduce(int[] arr){
        Arrays.sort(arr);
        int index=0;
        int count = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i]!=arr[index]) {
                if(index + 1 != i){
                    arr[count++]=arr[index];
                }
                index = i;
            }
        }
        int[] desArray=new int[count];
        if (desArray.length > 0) {
            System.arraycopy(arr, 0, desArray, 0, desArray.length);
        }
        return desArray;
    }
}
