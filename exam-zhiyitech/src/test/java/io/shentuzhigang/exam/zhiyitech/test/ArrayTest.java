package io.shentuzhigang.exam.zhiyitech.test;

import io.shentuzhigang.exam.zhiyitech.ArrayReduce;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-07 18:41
 */
public class ArrayTest {

    @Test
    public void test(){
        assertArrayEquals(new int[]{}, ArrayReduce.sortToReduce(new int[]{}));
        assertArrayEquals(new int[]{1,3,4},ArrayReduce.sortToReduce(new int[]{0, 1, 1, 2, 3, 4, 1, 3, 4, 5}));
        assertArrayEquals(new int[]{3,4},ArrayReduce.sortToReduce(new int[]{0, 1, 2, 3, 4, 3, 4, 5}));
    }
}
