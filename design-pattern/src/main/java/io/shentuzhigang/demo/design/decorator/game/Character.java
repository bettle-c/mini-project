package io.shentuzhigang.demo.design.decorator.game;


/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 14:22
 */
public interface Character {
    public void weapen();

    public void armour();
}

