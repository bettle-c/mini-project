package io.shentuzhigang.demo.design.singleton;

/**
 * 懒汉式（线程不安全）
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:10
 */
public class SingletonType3 {
    /**
     * 1、内部静态变量创建实例对象
     */
    private static SingletonType3 INSTANCE;

    /**
     * 2、构造器私有化，不能通过new创建对象
     */
    private SingletonType3() {

    }

    /**
     * 3. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType3 getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SingletonType3();
        }
        return INSTANCE;
    }
}
