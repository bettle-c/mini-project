package io.shentuzhigang.demo.design.singleton;

/**
 * 枚举
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:15
 */
public enum SingletonType8 {
    /**
     * 单例
     */
    INSTANCE;

    public void hello() {
        System.out.println("Hello Singleton");
    }
}
