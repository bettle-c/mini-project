package io.shentuzhigang.demo.design.decorator.game;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 14:22
 */
public class ConcreteCharacter implements Character {

    @Override
    public void weapen() {
        System.out.println("ConcreteCharacter+weapen");
    }

    @Override
    public void armour() {
        System.out.println("ConcreteCharacter+armour");
    }
}
