package io.shentuzhigang.demo.design.singleton;

/**
 * 饿汉式（静态变量）
 * 优点：
 * <p>
 * 缺点：
 * <p>
 * <p>
 * 结论：可用，内存浪费
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 15:53
 */
public class SingletonType1 {
    /**
     * 1、内部静态变量创建实例对象
     */
    private final static SingletonType1 INSTANCE = new SingletonType1();

    /**
     * 2、构造器私有化，不能通过new创建对象
     */
    private SingletonType1() {

    }

    /**
     * 3. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType1 getInstance() {
        return INSTANCE;
    }
}
