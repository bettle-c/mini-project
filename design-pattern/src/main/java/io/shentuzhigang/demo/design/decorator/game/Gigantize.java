package io.shentuzhigang.demo.design.decorator.game;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 14:22
 */
public class Gigantize extends Decorator {

    public Gigantize(Character character) {
        super(character);
    }

    public void furious() {
        System.out.println("furious");
    }

    @Override
    public void weapen() {
        super.weapen();
        System.out.println("Gigantize+weapen");
    }

    @Override
    public void armour() {
        super.armour();
        System.out.println("Gigantize+armour");
    }
}
