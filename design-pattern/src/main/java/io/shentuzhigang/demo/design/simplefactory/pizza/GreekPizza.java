package io.shentuzhigang.demo.design.simplefactory.pizza;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 17:11
 */
public class GreekPizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("prepare greek");
    }
}
