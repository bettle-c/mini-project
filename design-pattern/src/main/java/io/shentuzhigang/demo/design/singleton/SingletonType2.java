package io.shentuzhigang.demo.design.singleton;

/**
 * 饿汉式（静态代码块）
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 16:07
 */
public class SingletonType2 {
    /**
     * 1、内部静态变量创建实例对象
     */
    private final static SingletonType2 INSTANCE;

    /**
     * 2、静态代码块创建对象
     */
    static {
        //
        INSTANCE = new SingletonType2();
    }

    /**
     * 3、构造器私有化，不能通过new创建对象
     */
    private SingletonType2() {

    }

    /**
     * 4. 提供一个公有的静态方法，返回实例对象
     *
     * @return 实例对象
     */
    public static SingletonType2 getInstance() {
        return INSTANCE;
    }
}
