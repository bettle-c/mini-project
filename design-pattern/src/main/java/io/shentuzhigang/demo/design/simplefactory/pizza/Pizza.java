package io.shentuzhigang.demo.design.simplefactory.pizza;

/**
 * Pizza类
 *
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-08 17:06
 */
public abstract class Pizza {
    /**
     * 名字
     */
    protected String name;

    /**
     * 准备原材料
     */
    public abstract void prepare();

    public void bake() {
        System.out.println(name + " baking; ");
    }

    public void cut() {
        System.out.println(name + " cutting; ");
    }

    public void box() {
        System.out.println(name + " boxing; ");
    }

    public void setName(String name) {
        this.name = name;
    }
}
