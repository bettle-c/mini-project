package io.shentuzhigang.demo.design.producerAndconsumer;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-10 23:55
 */
public class Consumer extends Thread {
    /**
     * 每次消费的产品数量
     */
    private int num;
    /**
     * 所在放置的仓库
     */
    private Storage storage;

    /**
     * 构造函数，设置仓库
     *
     * @param storage
     */
    public Consumer(Storage storage) {
        this.storage = storage;
    }

    /**
     * 线程run函数
     */
    @Override
    public void run() {
        consume(num);
    }

    /**
     * 调用仓库Storage的生产函数
     *
     * @param num 数量
     */
    public void consume(int num) {
        storage.consume(num);
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }
}
