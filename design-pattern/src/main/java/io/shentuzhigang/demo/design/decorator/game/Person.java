package io.shentuzhigang.demo.design.decorator.game;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-19 14:22
 */
public class Person extends Decorator {

    public Person(Character character) {
        super(character);
    }

    @Override
    public void weapen() {
        super.weapen();
        System.out.println("Person+weapen");
    }

    @Override
    public void armour() {
        super.armour();
        System.out.println("Person+armour");
    }
}
