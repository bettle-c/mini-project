package io.shentuzhigang.demo.design.test.decorator;

import io.shentuzhigang.demo.design.decorator.game.Character;
import io.shentuzhigang.demo.design.decorator.game.*;
import org.junit.jupiter.api.Test;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-07 20:25
 */
public class GameTests {
    @Test
    public void t() {
        Character person = new ConcreteCharacter();
        Decorator decorator = new Gigantize(
                new Person(person));
        decorator.weapen();
        decorator.armour();
    }
}