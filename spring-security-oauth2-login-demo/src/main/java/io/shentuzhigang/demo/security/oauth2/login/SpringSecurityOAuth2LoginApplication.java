package io.shentuzhigang.demo.security.oauth2.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ShenTuZhiGang
 */
@SpringBootApplication
public class SpringSecurityOAuth2LoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityOAuth2LoginApplication.class, args);
    }

}
