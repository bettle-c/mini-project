package io.shentuzhigang.demo.security.oauth2.login.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-20 16:06
 */
@RestController
public class IndexController {
    @RequestMapping("/")
    public Object  index(){
        return "Hello World";
    }
}
