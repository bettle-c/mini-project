# Mini Project

## 概要介绍

Some Project, such as Demo Project、Small Project、Learn Program、Small Script or Test Project etc.

## 说明
### Demo Project
| 项目名称 | 项目描述 |
| ---- | ---- |
|hadoop-demo|Hadoop Demo|
|||

### Small Project
| 项目名称 | 项目描述 |
| ---- | ---- |
|||

### Learn Program
| 项目名称 | 项目描述 |
| ---- | ---- |
| design-pattern | 设计模式学习(JAVA) |
|||

### Small Script
| 项目名称 | 项目描述 |
| ---- | ---- |
|||

### Test Project
| 项目名称 | 项目描述 |
| ---- | ---- |
|test-whitebox-exp1|简单的白盒测试|
|test-blackbox-next-date|nextDate函数黑盒测试|
|||


---
可以点个Star再走吗？

