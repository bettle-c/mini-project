package io.shentuzhigang.demo.hadoop.mapreduce.flow.part;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.UUID;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 14:09
 */
public class FlowDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setPartitionerClass(CustomFlowPartitioner.class);
        job.setJarByClass(FlowDriver.class);
        job.setMapperClass(FlowMapper.class);
        job.setReducerClass(FlowReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowWritable.class);
        job.setNumReduceTasks(5);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.setInputPaths(job,new Path("hdfs://192.168.30.130:9000/flow.txt"));
        FileOutputFormat.setOutputPath(job,new Path("hdfs://192.168.30.130:9000/result"+ UUID.randomUUID().toString().replace("-","")));

        if (!job.waitForCompletion(true)) {
            return;
        }
    }
}
