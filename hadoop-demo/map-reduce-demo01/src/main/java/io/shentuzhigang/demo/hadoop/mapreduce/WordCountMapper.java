package io.shentuzhigang.demo.hadoop.mapreduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 10:11
 */
public class WordCountMapper extends Mapper<LongWritable, Text,Text, IntWritable> {

    @Override
    protected void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
//        super.map(key,value,context);
        String line = value.toString();
        String[] data = line.split(" ");
        for (String s : data) {
            context.write(new Text(s),new IntWritable(1));
        }
    }
}
