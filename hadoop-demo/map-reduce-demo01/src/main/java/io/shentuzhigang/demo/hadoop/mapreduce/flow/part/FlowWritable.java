package io.shentuzhigang.demo.hadoop.mapreduce.flow.part;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 13:52
 */
public class FlowWritable implements Writable {
    private int month;
    private String address;
    private String username;
    private int flow;

    @Override
    public String toString() {
        return "FLowWritable{" +
                "month=" + month +
                ", address='" + address + '\'' +
                ", username='" + username + '\'' +
                ", flow=" + flow +
                '}';
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getFlow() {
        return flow;
    }

    public void setFlow(int flow) {
        this.flow = flow;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(this.month);
        dataOutput.writeUTF(this.address);
        dataOutput.writeUTF(this.username);
        dataOutput.writeInt(this.flow);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.month = dataInput.readInt();
        this.address = dataInput.readUTF();
        this.username = dataInput.readUTF();
        this.flow = dataInput.readInt();
    }
}
