package io.shentuzhigang.demo.hadoop.mapreduce.flow;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-01-20 14:02
 */
public class FlowMapper extends Mapper<LongWritable, Text,Text,FlowWritable> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] data = value.toString().split(" ");
        FlowWritable  flow = new FlowWritable();
        flow.setMonth(Integer.parseInt(data[0]));
        flow.setAddress(data[1]);
        flow.setUsername(data[2]);
        flow.setFlow(Integer.parseInt(data[3]));
        context.write(new Text(flow.getUsername()),flow);
    }
}
