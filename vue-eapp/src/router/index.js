import {createRouter, createWebHistory} from 'vue-router'

const routes = [
  {
    path: '/',
    redirect: '/shop',
  },
  {
    path: '/shop',
    redirect: '/shop/goods',
    component: () => import('@/views/shop'),
    children: [
      {
        path: 'goods',
        component: () => import('@/views/shop/goods')
      },
      {
        path: 'seller',
        component: () => import('@/views/shop/seller')
      },
      {
        path: 'ratings',
        component: () => import( '@/views/shop/ratings')
      }
    ]
  },
  {
    path: '/food',
    component: () => import('@/views/food'),
    props:true,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
