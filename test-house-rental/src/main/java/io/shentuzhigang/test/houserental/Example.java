package io.shentuzhigang.test.houserental;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-20 15:00
 */
public class Example {

    // Mooctest Selenium Example


    // <!> Check if selenium-standalone.jar is added to build path.

    public static void test(WebDriver driver) {
        // TODO Test script
        // eg:driver.get("https://www.baidu.com/")
        // eg:driver.findElement(By.id("wd"));
        driver.get("http://114.215.176.95:60513/text2/");
        driver.manage().window().setSize(new Dimension(1552, 840));
        driver.findElement(By.name("username")).sendKeys("admin");
        driver.findElement(By.name("password")).sendKeys("123456");
        driver.findElement(By.id("login-button")).click();
        driver.findElement(By.cssSelector(".container")).click();
        driver.findElement(By.linkText("房源列表")).click();
        driver.findElement(By.linkText("修改")).click();
        driver.findElement(By.id("price")).clear();
        driver.findElement(By.id("price")).sendKeys("700");
        driver.findElement(By.cssSelector(".btn-primary")).click();
        driver.findElement(By.linkText("添加房源")).click();
        driver.findElement(By.id("houseid")).sendKeys("a100861");
        driver.findElement(By.id("address")).sendKeys("615黄记煌三汁焖锅");
        driver.findElement(By.id("area")).sendKeys("1000");
        driver.findElement(By.id("price")).sendKeys("700.0");
        {
            WebElement dropdown = driver.findElement(By.id("status"));
            dropdown.findElement(By.xpath("//option[. = '未租赁']")).click();
        }
        driver.findElement(By.id("status")).click();
        driver.findElement(By.cssSelector(".btn-primary")).click();
        driver.findElement(By.linkText("租赁及合同信息")).click();
        driver.findElement(By.linkText("在租列表")).click();
        driver.findElement(By.linkText("查看合同")).click();
        driver.findElement(By.name("hetong")).click();
        driver.findElement(By.cssSelector(".btn-primary")).click();
        driver.findElement(By.cssSelector("tr:nth-child(10)")).click();
        driver.findElement(By.id("payday")).clear();
        driver.findElement(By.id("payday")).sendKeys("2");
        driver.findElement(By.cssSelector(".btn-primary")).click();
        driver.findElement(By.linkText("租客已缴租金")).click();
        driver.findElement(By.id("zuname")).sendKeys("cwy");
        driver.findElement(By.name("sub")).click();
        driver.findElement(By.linkText("我要收租")).click();
        driver.findElement(By.linkText("收租")).click();
        driver.findElement(By.id("date")).click();
        driver.findElement(By.linkText("11")).click();
        driver.findElement(By.id("zuke")).clear();
        driver.findElement(By.id("zuke")).sendKeys("700.0");
        driver.findElement(By.cssSelector(".btn-primary")).click();
    }

    public static void main(String[] args) {
        // Run main function to test your script.
        WebDriver driver = new ChromeDriver();
        try { test(driver); }
        catch(Exception e) { e.printStackTrace(); }
        finally { driver.quit(); }
    }

}
