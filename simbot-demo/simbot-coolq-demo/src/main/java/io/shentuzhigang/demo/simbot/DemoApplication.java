package io.shentuzhigang.demo.simbot;

import com.forte.component.forcoolqhttpapi.CoolQHttpApplication;
import com.forte.qqrobot.SimpleRobotApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SimpleRobotApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        CoolQHttpApplication application = new CoolQHttpApplication();
// 启动, 因为没有继承接口，并且使用注解，于是直接使用RunApp的类进行启动就好了
        application.run(DemoApplication.class);
    }

}
