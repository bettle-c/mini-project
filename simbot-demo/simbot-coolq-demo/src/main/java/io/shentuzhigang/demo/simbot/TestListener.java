package io.shentuzhigang.demo.simbot;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.msgget.PrivateMsg;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.sender.senderlist.SenderSendList;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-06-14 15:53
 */
@Beans
public class TestListener implements SenderSendList {

    @Listen(MsgGetTypes.privateMsg)
    @Filter("hello.*")
    public void testListen1(PrivateMsg msg, MsgSender sender) {
        System.out.println(msg);
        // 以下三种方法均可，效果相同
        System.out.println(sender.SENDER.sendPrivateMsg(msg, msg.getMsg()));
//        sender.SENDER.sendPrivateMsg(msg.getQQ(), msg.getMsg());
//        sender.SENDER.sendPrivateMsg(msg.getQQCode(), msg.getMsg());
    }
    @Listen(MsgGetTypes.groupMsg)
    @Filter(".*")
    public void testListen1(GroupMsg msg, MsgSender sender) {
        System.out.println(msg);
        // 以下三种方法均可，效果相同
        System.out.println(sender.SENDER.sendGroupMsg(msg, msg.getMsg()));
    //        sender.SENDER.sendPrivateMsg(msg.getQQ(), msg.getMsg());
    //        sender.SENDER.sendPrivateMsg(msg.getQQCode(), msg.getMsg());
    }
    @Override
    public String sendDiscussMsg(String group, String msg) {
        return msg;
    }

    @Override
    public String sendGroupMsg(String group, String msg) {
        return null;
    }

    @Override
    public String sendPrivateMsg(String QQ, String msg) {
        return msg;
    }

    @Override
    public boolean sendFlower(String group, String QQ) {
        return false;
    }

    @Override
    public boolean sendLike(String QQ, int times) {
        return false;
    }

    @Override
    public boolean sendGroupNotice(String group, String title, String text, boolean top, boolean toNewMember, boolean confirm) {
        return false;
    }
}
