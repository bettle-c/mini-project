import cv2
import pytesseract
from pytesseract import Output
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import numpy as np

def noise_remove_pil(img, k):
    """
    8邻域降噪
    Args:
        image_name: 图片文件命名
        k: 判断阈值

    Returns:

    """

    def calculate_noise_count(img_obj, w, h):
        """
        计算邻域非白色的个数
        Args:
            img_obj: img obj
            w: width
            h: height
        Returns:
            count (int)
        """
        count = 0
        width, height = img_obj.size
        for _w_ in [w - 1, w, w + 1]:
            for _h_ in [h - 1, h, h + 1]:
                if _w_ > width - 1:
                    continue
                if _h_ > height - 1:
                    continue
                if _w_ == w and _h_ == h:
                    continue
                if img_obj.getpixel((_w_, _h_)) < 230:  # 这里因为是灰度图像，设置小于230为非白色
                    count += 1
        return count


    # 灰度
    gray_img = img.convert('L')

    w, h = gray_img.size
    for _w in range(w):
        for _h in range(h):
            if _w == 0 or _h == 0:
                gray_img.putpixel((_w, _h), 255)
                continue
            # 计算邻域非白色的个数
            pixel = gray_img.getpixel((_w, _h))
            if pixel == 255:
                continue

            if calculate_noise_count(gray_img, _w, _h) < k:
                gray_img.putpixel((_w, _h), 255)
    return gray_img


def recoText(im):
    """
    识别字符并返回所识别的字符及它们的坐标
    :param im: 需要识别的图片
    :return data: 字符及它们在图片的位置
    """
    data = {}
    d = pytesseract.image_to_data(im, output_type=Output.DICT, lang='chi_sim')
    for i in range(len(d['text'])):
        if 0 < len(d['text'][i]):
            (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
            data[d['text'][i]] = (x, y, w, h)
            # data[d['text'][i]] = ([d['left'][i], d['top'][i], d['width'][i], d['height'][i]])
            print(x, y, w, h)
            cv2.rectangle(im, (x, y), (x + w, y + h), (255, 0, 0), 1)
            # 使用cv2.putText不能显示中文，需要使用下面的代码代替
            # cv2.putText(im, d['text'][i], (x, y-8), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)

            pilimg = Image.fromarray(im)
            pilimg.resize((800, 600),Image.ANTIALIAS)
            draw = ImageDraw.Draw(pilimg)
            # 参数1：字体文件路径，参数2：字体大小
            font = ImageFont.truetype("simhei.ttf", 15, encoding="utf-8")
            # 参数1：打印坐标，参数2：文本，参数3：字体颜色，参数4：字体
            draw.text((x, y - 10), d['text'][i], (255, 0, 0), font=font)
            im = cv2.cvtColor(np.array(pilimg), cv2.COLOR_RGB2BGR)

    cv2.imshow("recoText", im)
    return data


if __name__ == '__main__':
    img = Image.open(r'D:\Code\Contest\服务外包\比赛\中国大学生服务外包创新创业大赛\第十二届中国大学生服务外包创新创业大赛\A18\ocr测试集\识别测试集\images_2k\images_2\5f1fbb9cffeca5baa9c9b2bf-17.jpg')
    #img = noise_remove_pil(img,4)

    # im_gray1 = img.convert('L')
    # print(im_gray1)
    # im_gray1 = np.array(im_gray1)
    # print(im_gray1)
    # avg_gray = np.average(im_gray1)
    # im_gray1 = np.where(im_gray1[..., :] < avg_gray, 0, 255)
    # print(im_gray1)
    # # cv2.imshow("src", img)
    # # im_gray1 = np.asarray(im_gray1,dtype=np.uint8)
    img = img.convert("RGB")
    # print(img)
    # img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)
    # cv2.imshow("recoText", img)
    data = recoText(np.array(img))
    print(pytesseract.image_to_string(img, lang="chi_sim"))

    cv2.waitKey(0)
    cv2.destroyAllWindows()