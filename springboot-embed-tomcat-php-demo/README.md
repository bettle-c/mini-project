# Spring Boot 内置Tomcat——集成PHP解决方案

## 问题分析
### 一、安装PHP

> PHP安装与配置：https://www.php.net/manual/zh/install.php

### 二、Spring Boot 自定义Servlet容器

> WebServerFactoryCustomizer：https://blog.csdn.net/qq_45235291/article/details/95921083
```java
    @Bean
    public WebServerFactoryCustomizer webServerFactoryCustomizer() {
        return new WebServerFactoryCustomizer<TomcatServletWebServerFactory>() {
            @Override
            public void customize(TomcatServletWebServerFactory factory) {
                factory.setPort(8090);
            }
        };
    }
```
> 底层原理：
> ![](https://img-blog.csdnimg.cn/20201103234841742.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)
> org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
> ![](https://img-blog.csdnimg.cn/20201103235414451.png)


### 三、Spring Boot配置JSP

org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
```java
	protected void prepareContext(Host host, ServletContextInitializer[] initializers) {
		...
		if (shouldRegisterJspServlet()) {
			addJspServlet(context);
			addJasperInitializer(context);
		}
		...
	}
	private void addJspServlet(Context context) {
		Wrapper jspServlet = context.createWrapper();
		jspServlet.setName("jsp");
		jspServlet.setServletClass(getJsp().getClassName());
		jspServlet.addInitParameter("fork", "false");
		getJsp().getInitParameters().forEach(jspServlet::addInitParameter);
		jspServlet.setLoadOnStartup(3);
		context.addChild(jspServlet);
		context.addServletMappingDecoded("*.jsp", "jsp");
		context.addServletMappingDecoded("*.jspx", "jsp");
	}

	private void addJasperInitializer(TomcatEmbeddedContext context) {
		try {
			ServletContainerInitializer initializer = (ServletContainerInitializer) ClassUtils
					.forName("org.apache.jasper.servlet.JasperInitializer", null).newInstance();
			context.addServletContainerInitializer(initializer, null);
		}
		catch (Exception ex) {
			// Probably not Tomcat 8
		}
	}
```

## 解决方案
### 方法一：通过org.apache.catalina.servlets.CGIServlet实现

根据Spring Boot中JSP的配置PHP和普通Tomcat服务器配置PHP的方式编写对Spring Boot 内置Tomcat的自定义配置
```java
package io.shentuzhigang.demo.php.config;

import org.apache.catalina.Wrapper;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.Jsp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ShenTuZhiGang
 * @version 1.1.0
 * @date 2020-11-02 22:42
 */
@Configuration
public class CustomTomcatConfig {
    @Bean
    public WebServerFactoryCustomizer webServerFactoryCustomizer() {
        return (WebServerFactoryCustomizer<TomcatServletWebServerFactory>) factory ->
                factory.addContextCustomizers(context -> {
                    Jsp jsp = new Jsp();
                    jsp.setClassName("org.apache.catalina.servlets.DefaultServlet");
                    Wrapper jspServlet = context.createWrapper();
                    jspServlet.setName("jsp");
                    jspServlet.setServletClass(jsp.getClassName());
                    jspServlet.addInitParameter("fork", "false");
                    jsp.getInitParameters().forEach(jspServlet::addInitParameter);
                    jspServlet.setLoadOnStartup(3);
                    context.addChild(jspServlet);
                    context.addServletMappingDecoded("*.jsp", "jsp");
                    context.addServletMappingDecoded("*.jspx", "jsp");
                    context.setPrivileged(true);

                    Php php = new Php();
                    php.setClassName("org.apache.catalina.servlets.CGIServlet");
                    Wrapper phpServlet = context.createWrapper();
                    phpServlet.setName("php");

                    phpServlet.setServletClass(php.getClassName());
                    phpServlet.addInitParameter("fork", "false");
                    php.getInitParameters().forEach(phpServlet::addInitParameter);
                    phpServlet.setLoadOnStartup(4);
                    context.addChild(phpServlet);
                    context.addServletMappingDecoded("*.php", "php");
                    context.addServletMappingDecoded("*.phpx", "php");
                });
    }
    public class Php {

        /**
         * Class name of the servlet to use for JSPs. If registered is true and this class is
         * on the classpath then it will be registered.
         */
        private String className = "org.apache.catalina.servlets.CGIServlet";

        private Map<String, String> initParameters = new HashMap<>();

        /**
         * Whether the JSP servlet is registered.
         */
        private boolean registered = true;

        public Php() {
            this.initParameters.put("development", "false");
            this.initParameters.put("clientInputTimeout","200");
            this.initParameters.put("debug","0");
            this.initParameters.put("executable","D:\\ext\\php\\php-cgi.exe");
            this.initParameters.put("passShellEnvironment","true");
            this.initParameters.put("cgiPathPrefix","");
        }

        /**
         * Return the class name of the servlet to use for JSPs. If {@link #getRegistered()
         * registered} is {@code true} and this class is on the classpath then it will be
         * registered.
         * @return the class name of the servlet to use for JSPs
         */
        public String getClassName() {
            return this.className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        /**
         * Return the init parameters used to configure the JSP servlet.
         * @return the init parameters
         */
        public Map<String, String> getInitParameters() {
            return this.initParameters;
        }

        public void setInitParameters(Map<String, String> initParameters) {
            this.initParameters = initParameters;
        }

        /**
         * Return whether the JSP servlet is registered.
         * @return {@code true} to register the JSP servlet
         */
        public boolean getRegistered() {
            return this.registered;
        }

        public void setRegistered(boolean registered) {
            this.registered = registered;
        }

    }

}

```
> 说明：
>
> cgiPathPrefix：指定所需要访问的 php 文件所在的文件夹（按自己的需求设置）；
>
> executable：指定本地 php 环境的安装目录（注意区分 windows 和 linux）
>
> 其它的配置是 servlet 相关的，熟悉 javaee 的人应该都很清楚，/cgi-bin/* 指定了 浏览器 url 访问的前缀；

将`IntelliJ IDEA`中[配置模块目录设为文档根目录(DocumentRoot)](https://shentuzhigang.blog.csdn.net/article/details/109480082)

否则会出现以下问题：
![](https://img-blog.csdnimg.cn/20201103231623314.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)

```bash
2020-11-03 22:51:55.644 DEBUG 52604 --- [nio-8090-exec-5] o.a.catalina.connector.CoyoteAdapter     :  Requested cookie session id is EF152E88F50BBB1FEB5DE7EC1EFD2F9C
2020-11-03 22:51:55.645 DEBUG 52604 --- [nio-8090-exec-5] o.a.c.authenticator.AuthenticatorBase    : Security checking request GET /MyZSTU/index.php
2020-11-03 22:51:55.645 DEBUG 52604 --- [nio-8090-exec-5] org.apache.catalina.realm.RealmBase      :   No applicable constraints defined
2020-11-03 22:51:55.645 DEBUG 52604 --- [nio-8090-exec-5] o.a.c.authenticator.AuthenticatorBase    : Not subject to any constraint
2020-11-03 22:51:55.654 DEBUG 52604 --- [nio-8090-exec-5] org.apache.catalina.servlets.CGIServlet  : CGI script requested at path [/index.php] relative to CGI location [C:\Users\Lenovo\AppData\Local\Temp\tomcat-docbase.6285430797231421632.8090\]
2020-11-03 22:51:55.655 DEBUG 52604 --- [nio-8090-exec-5] org.apache.catalina.servlets.CGIServlet  : Looking for a file at [C:\Users\Lenovo\AppData\Local\Temp\tomcat-docbase.6285430797231421632.8090]
2020-11-03 22:51:55.655 DEBUG 52604 --- [nio-8090-exec-5] org.apache.catalina.servlets.CGIServlet  : Looking for a file at [C:\Users\Lenovo\AppData\Local\Temp\tomcat-docbase.6285430797231421632.8090\index.php]
2020-11-03 22:51:55.656 DEBUG 52604 --- [nio-8090-exec-5] o.a.c.c.C.[Tomcat].[localhost]           : Processing ErrorPage[errorCode=0, location=/error]
2020-11-03 22:51:55.684 DEBUG 52604 --- [nio-8090-exec-5] o.a.c.c.C.[.[.[.[dispatcherServlet]      :  Disabling the response for further output
```

#### 编写PHP

> 注意目录：
> ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210606205349251.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)

![](https://img-blog.csdnimg.cn/20201103231451713.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)


#### 访问
![](https://img-blog.csdnimg.cn/2020110323174654.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)


#### 日志
```bash
2020-11-03 23:57:16.515 DEBUG 31272 --- [nio-8090-exec-6] o.a.catalina.connector.CoyoteAdapter     :  Requested cookie session id is EF152E88F50BBB1FEB5DE7EC1EFD2F9C
2020-11-03 23:57:16.516 DEBUG 31272 --- [nio-8090-exec-6] o.a.c.authenticator.AuthenticatorBase    : Security checking request GET /MyZSTU/index.php
2020-11-03 23:57:16.516 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.realm.RealmBase      :   No applicable constraints defined
2020-11-03 23:57:16.516 DEBUG 31272 --- [nio-8090-exec-6] o.a.c.authenticator.AuthenticatorBase    : Not subject to any constraint
2020-11-03 23:57:16.521 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.servlets.CGIServlet  : CGI script requested at path [/index.php] relative to CGI location [E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\]
2020-11-03 23:57:16.522 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.servlets.CGIServlet  : Looking for a file at [E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp]
2020-11-03 23:57:16.522 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.servlets.CGIServlet  : Looking for a file at [E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\index.php]
2020-11-03 23:57:16.522 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.servlets.CGIServlet  : Found CGI: name [index.php], path [E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\index.php], script name [/MyZSTU/index.php] and CGI name [/index.php]
2020-11-03 23:57:16.524 DEBUG 31272 --- [nio-8090-exec-6] org.apache.catalina.servlets.CGIServlet  : envp: [{PSModulePath=D:\Program Files\WindowsPowerShell\Modules;C:\windows\system32\WindowsPowerShell\v1.0\Modules, CLASSPATH=.;D:\Program Files\Apache Software Foundation\Tomcat 8.5\lib\servlet-api.jar, REQUEST_URI=/MyZSTU/index.php, GATEWAY_INTERFACE=CGI/1.1, ProgramData=C:\ProgramData, SESSIONNAME=Console, NIEXTCCOMPILERSUPP=C:\Program Files (x86)\National Instruments\Shared\ExternalCompilerSupport\C\, SERVER_NAME=localhost, MIC_LD_LIBRARY_PATH=C:\Program Files (x86)\Common Files\Intel\Shared Libraries\compiler\lib\mic, USERDOMAIN=LAPTOP-7ASLMDHA, X_TOMCAT_SCRIPT_PATH=E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\index.php, REQUEST_METHOD=GET, CATALINA_HOME=D:\Program Files\Apache Software Foundation\Tomcat 8.5, REMOTE_ADDR=0:0:0:0:0:0:0:1, USERNAME=ShenTuZhiGang, APPDATA=C:\Users\Lenovo\AppData\Roaming, LUA_PATH=;;D:\Program Files (x86)\Lua\5.1\lua\?.luac, SERVER_SOFTWARE=TOMCAT, CommonProgramFiles(x86)=C:\Program Files (x86)\Common Files, HOMEPATH=\Users\Lenovo, PROCESSOR_LEVEL=6, CONTENT_LENGTH=, USERDOMAIN_ROAMINGPROFILE=LAPTOP-7ASLMDHA, PROCESSOR_IDENTIFIER=Intel64 Family 6 Model 158 Stepping 10, GenuineIntel, LOGONSERVER=\\LAPTOP-7ASLMDHA, PROCESSOR_ARCHITECTURE=AMD64, HTTP_ACCEPT_ENCODING=gzip, deflate, br, Path=D:\ProgramData\Anaconda3;D:\ProgramData\Anaconda3\Library\mingw-w64\bin;D:\ProgramData\Anaconda3\Library\usr\bin;D:\ProgramData\Anaconda3\Library\bin;D:\ProgramData\Anaconda3\Scripts;D:\ProgramData\Anaconda2;D:\ProgramData\Anaconda2\Library\mingw-w64\bin;D:\ProgramData\Anaconda2\Library\usr\bin;D:\ProgramData\Anaconda2\Library\bin;D:\ProgramData\Anaconda2\Scripts;C:\Program Files (x86)\Common Files\Intel\Shared Libraries\redist\intel64\compiler;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\iCLS\;C:\Program Files\Intel\Intel(R) Management Engine Components\iCLS\;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\IPT;C:\Program Files\Intel\Intel(R) Management Engine Components\IPT;D:\Program Files (x86)\TDM GCC\bin;C:\Program Files\Java\jdk1.8.0_181\bin;C:\Program Files\NVIDIA Corporation\NVIDIA NvDLISR;D:\Program Files (x86)\doxygen\bin;D:\PROGRA~1\ATT\Graphviz\bin;D:\Program Files\010 Editor;D:\Program Files\Apache Software Foundation\Tomcat 8.5\bin;D:\Program Files\nodejs\;D:\Program Files (x86)\Maven\apache-maven-3.6.2\bin;D:\Program Files\Git\bin;D:\Program Files\Microsoft SQL Server\130\Tools\Binn\;D:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\;D:\Program Files\dotnet\;D:\Program Files\erl10.7\bin;D:\Program Files\Redis\;C:\Program Files (x86)\NVIDIA Corporation\PhysX\Common;D:\Program Files\NVIDIA Corporation\NVIDIA NvDLISR;D:\Program Files (x86)\Lua\5.1;D:\Program Files (x86)\Lua\5.1\clibs;C:\Users\Lenovo\AppData\Local\Programs\Python\Python37\Scripts\;C:\Users\Lenovo\AppData\Local\Programs\Python\Python37\;C:\Users\Lenovo\AppData\Local\Microsoft\WindowsApps;D:\Program Files (x86)\JetBrains\PyCharm 2018.3.2\bin;D:\Program Files (x86)\JetBrains\IntelliJ IDEA 2018.3.5\bin;D:\Program Files\Microsoft VS Code\bin;D:\Program Files\JetBrains\WebStorm 2019.2\bin;C:\Users\Lenovo\AppData\Roaming\npm;C:\Users\Lenovo\AppData\Local\GitHubDesktop\bin;D:\Program Files\JetBrains\DataGrip 2019.2.5\bin;, HTTP_ACCEPT_LANGUAGE=zh-CN,zh;q=0.9, REMOTE_IDENT=, NUMBER_OF_PROCESSORS=8, Git_HOME=D:\Program Files\Git, REMOTE_HOST=0:0:0:0:0:0:0:1, SCRIPT_FILENAME=E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\index.php, PUBLIC=C:\Users\Public, CATALINA_BASE=D:\Program Files\Apache Software Foundation\Tomcat 8.5, QT_DEVICE_PIXEL_RATIO=auto, ERLANG_HOME=D:\Program Files\erl10.7, REMOTE_USER=, =::=::\, USERPROFILE=C:\Users\Lenovo, CONTENT_TYPE=, PATH_INFO=, HTTP_HOST=localhost:8090, MAVEN_HOME=D:\Program Files (x86)\Maven\apache-maven-3.6.2, INTEL_DEV_REDIST=C:\Program Files (x86)\Common Files\Intel\Shared Libraries\, COMPUTERNAME=LAPTOP-7ASLMDHA, OS=Windows_NT, AUTH_TYPE=, configsetroot=C:\windows\ConfigSetRoot, CommonProgramW6432=C:\Program Files\Common Files, SCRIPT_NAME=/MyZSTU/index.php, HOMEDRIVE=C:, SERVER_PROTOCOL=HTTP/1.1, IntelliJ IDEA=D:\Program Files (x86)\JetBrains\IntelliJ IDEA 2018.3.5\bin;, HTTP_CACHE_CONTROL=max-age=0, windir=C:\windows, WebStorm=D:\Program Files\JetBrains\WebStorm 2019.2\bin;, PROCESSOR_REVISION=9e0a, LOCALAPPDATA=C:\Users\Lenovo\AppData\Local, CommonProgramFiles=C:\Program Files\Common Files, HTTP_ACCEPT=text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3, OneDrive=C:\Users\Lenovo\OneDrive, DriverData=C:\Windows\System32\Drivers\DriverData, OneDriveConsumer=C:\Users\Lenovo\OneDrive, ComSpec=C:\windows\system32\cmd.exe, SERVER_PORT=8090, ALLUSERSPROFILE=C:\ProgramData, PyCharm=D:\Program Files (x86)\JetBrains\PyCharm 2018.3.2\bin;, SystemDrive=C:, ProgramW6432=D:\Program Files, DataGrip=D:\Program Files\JetBrains\DataGrip 2019.2.5\bin;, ProgramFiles(x86)=D:\Program Files (x86), JAVA_HOME=C:\Program Files\Java\jdk1.8.0_181, TOMCAT_HOME=D:\Program Files\Apache Software Foundation\Tomcat 8.5, HTTP_USER_AGENT=Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36, PATHEXT=.COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC;.wlua;.lexe, ProgramFiles=D:\Program Files, PT7HOME=D:\Program Files\Cisco Packet Tracer 7.2.1, HTTP_COOKIE=JSESSIONID=EF152E88F50BBB1FEB5DE7EC1EFD2F9C, SystemRoot=C:\windows, LUA_DEV=D:\Program Files (x86)\Lua\5.1, TMP=C:\Users\Lenovo\AppData\Local\Temp, QUERY_STRING=, TEMP=C:\Users\Lenovo\AppData\Local\Temp}], command: [E:\Code\Project\JAVA\DEMO_Project\myzstu\myzstu-web\src\main\webapp\index.php]
```


### 方法二：PHP/Java Bridge



## 参考文章
[Spring Boot 内置Tomcat——集成PHP解决方案](https://shentuzhigang.blog.csdn.net/article/details/117636436)