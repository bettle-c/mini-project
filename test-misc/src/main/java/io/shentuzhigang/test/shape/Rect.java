package io.shentuzhigang.test.shape;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-12-24 13:41
 */
public class Rect {
    private Point2d topLeftPoint;
    private int width;
    private int height;

    public Rect() {
        this(new Point2d(0, 0), 1, 1);
    }

    public Rect(Point2d topLeftPoint) {
        this(topLeftPoint, 1, 1);
    }

    public Rect(Point2d topLeftPoint, int weight, int height) {
        this.topLeftPoint = topLeftPoint;
        this.width = weight;
        this.height = height;
    }

    public double area() {
        return width * height;
    }

    public boolean contain(Point2d point) {
        return topLeftPoint.getX() <= point.getX() && point.getX() <= topLeftPoint.getX() + width
                && topLeftPoint.getY() <= point.getY() && point.getY() <= topLeftPoint.getY() + height;
    }

    public Point2d getTopLeftPoint() {
        return topLeftPoint;
    }

    public void setTopLeftPoint(Point2d topLeftPoint) {
        this.topLeftPoint = topLeftPoint;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
