package io.shentuzhigang.learn.jvm.volatilelearn;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-05 21:12
 */
public class VolatileSerialDemo {

    static int x = 0;
    static int y = 0;
    static int a = 0;
    static int b = 0;

    public static void main(String[] args) throws InterruptedException {
        Set<String> resultSet = new HashSet<>();
        for (int i = 0; i < 10000000; i++) {
            x = 0;
            y = 0;
            a = 0;
            b = 0;
            Thread one = new Thread(() -> {
                a = y;
                x = 1;
            }, "one");
            Thread other = new Thread(() -> {
                b = x;
                y = 1;
            }, "other");

            one.start();
            other.start();
            one.join();
            other.join();

            resultSet.add("a=" + a + "," + "b=" + b);
            System.out.println(resultSet);
        }
    }
}
