package io.shentuzhigang.learn.jvm.volatilelearn;

import java.lang.InterruptedException;

/**
 * 缓存一致性测试
 * @author ShenTuZhiGang
 * @email 1600337300@qq.com
 * @version 1.0.0
 * @date 2021-06-05 18:47
 */
public class VolatileVisibolityDemo {
    private static volatile boolean initFlag = false;
    // private static boolean initFlag = false;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            System.out.println("waiting data...");
            while (!initFlag) {
            }
            System.out.println("=====success");
        }).start();

        Thread.sleep(2000);

        new Thread(() -> {
            System.out.println("prepare data begin...");
            initFlag = true;
            System.out.println("prepare data end...");
        }).start();
    }
}