package io.shentuzhigang.test.sorting;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SortingTest {
    private final Sorting sorting;
    private int[][] data;

    public SortingTest() {
        this.sorting = new Sorting();
    }

    @Before
    public void setUp() {
        this.data = new int[][]{
                {1, 2, 3, 4, 5},
                {2, 1, 4, 5, 3},
                {3, 4, 1, 2, 5},
                {5, 4, 3, 2, 1},
                {},
                {1},
                {9, 6, 3, 2, 5, 1, 8, 5, 4, 0, 10, -2},
                {5,4,3,2,6,1,7,9,8,10,12,13,11}
        };
    }

    @Test
    public void insertionSortTest() {
        for (int[] a : data) {
            sorting.insertionSort(a);
        }
    }

    @Test
    public void quicksortTest() {
        for (int[] a : data) {
            Sorting.quicksort(a);
        }
    }

    @After
    public void setDown(){
        for (int[] a : data) {
            assertTrue(sorting.isSorted(a));
        }
        assertFalse(sorting.isSorted(new int[]{2, 1}));
        Sorting.swapReferences(new Object[]{2, 1}, 0, 1);
    }
}
