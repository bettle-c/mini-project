package io.shentuzhigang.hack.hongqingting.http.interceptor;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-08-25 15:33
 */
public class LogInterceptor implements Interceptor {
    private static final Logger log = LoggerFactory.getLogger(LogInterceptor.class);


    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        //获得请求信息，此处如有需要可以添加headers信息
        Request request = chain.request();

        /*Cookie*/
        //request.newBuilder().addHeader("Cookie", "aaaa");

        log.debug("[request]:" + request.toString());
        log.debug("[request-headers]:\n" + request.headers().toString());

        if (log.isDebugEnabled()) {
            readRequestBody(request);
        }

        /* 记录请求耗时 */
        long startNs = System.nanoTime();
        /* 发送请求，获得响应 */
        Response response = chain.proceed(request);

        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
        /* 打印请求耗时 */
        log.debug("[耗时]:" + tookMs + "ms");

        /* 使用response获得headers()*/
        log.debug("[response-code]:" + response.code());
        log.debug("[response-headers]:\n" + response.headers().toString());

        if (log.isDebugEnabled()) {
            readResponseBody(response);
        }

        /*更新本地Cookie*/

        return response;
    }

    /**
     * 打印RequestBody
     *
     * @param req RequestBody
     */
    private void readRequestBody(Request req) {
        if (req.body() == null) {
            return;
        }
        Request request = req.newBuilder().build();
        Buffer buffer = new Buffer();
        try {
            request.body().writeTo(buffer);
            log.debug(buffer.readUtf8());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印ResponseBody
     *
     * @param res ResponseBody
     */
    private void readResponseBody(Response res) {
        /* 获得返回的body，注意此处不要使用responseBody.string()获取返回数据，原因在于这个方法会消耗返回结果的数据(buffer) */
        ResponseBody responseBody = res.body();
        /* 为了不消耗buffer，我们这里使用source先获得buffer对象，然后clone()后使用 */
        BufferedSource source = responseBody.source();
        try {
            // Buffer the entire body.
            source.request(Long.MAX_VALUE);
            /* 获得返回的数据 */
            Buffer buffer = source.getBuffer();
            if (buffer.size() < 1024) {
                /* 使用前clone() 下，避免直接消耗 */
                log.trace("[response-body]:" + buffer.clone().readString(Charset.forName("UTF-8")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
