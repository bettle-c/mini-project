[English](README_en.md) | 简体中文

> **代码仓库**: https://gitee.com/shentuzhigang/covid-19-map
>
> **Blog**: https://shentuzhigang.blog.csdn.net/article/details/116157059

# 实验目标
1.	掌握使用Vue-CLI脚手架工具在自己的电脑上建立项目，并运行调试工具。
2.	学习使用ECharts图表开源库开发地图展示功能，了解图表开发方法。基本用法参考ECharts官网：https://www.echartsjs.com/zh/feature.html

# 实验内容
1. Vue-CLI脚手架工具搭建一个Web项目covid-19-map_demo。
2. 使用Vue.js和ECharts编写一个疫情地图显示网页。内容及格式可自己定
效果可参照丁香园疫情地图：https://ncov.dxy.cn/ncovh5/view/pneumonia


# 实验详细过程和步骤
## 截图展示
![截图展示](./doc/image/result.png)
## 主要代码及实现方法简介
### 创建项目
先安装好开发工具：node.js、vue/cli4、webpack
使用vue/cli4搭建项目，命令：

```bash
vue create vue-ditu-3
```
添加Vue-Router，命令：
```bash
vue add router
```
使用vue/cli脚手架搭建好项目后，运行项目，命令：

```bash
npm run serve
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426152816172.png)

### 封装自定义组件
在views文件夹下新建一个自定义的组件Echarts.vue

```html
<template>
  <div>

  </div>
</template>

<script>
  export default {
    name: "EChart",
    data() {
      return {}
    }
  }
</script>

<style scoped>

</style>

```

### 配置路由
在router文件下的index.js里配置自定义的组件Echarts.vue对应的路由
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426155755955.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)

然后打开APP.vue，配置如下图所示的路由跳转:
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021042615584322.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)


> 在路由配置时，你也可以使用另一种路由配置方式
 ```js
import ECharts from "../views/ECharts";
const routes = [
  {
    path: '/echarts',
    name: 'ECharts',
    component: ECharts
  }
]
```


配置好路由后，点击Echarts就能看到如下图所示效果:
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426160224818.png)

### ECharts的简介

> ECharts，缩写来自 Enterprise Charts，商业级数据图表，是百度的一个开源的数据可视化工具，一个纯Javascript的图表库，目前很多商业项目都在使用。
> Echarts的官网是：https://www.echartsjs.com/zh/index.html
> 官网地图案例：https://www.echartsjs.com/examples/en/editor.html?c=doc-example/map-visualMap-pieces&edit=1
> 你可以在案例左边修改数据和参数，右边就可以实时显示效果。

### ECharts的安装
在项目中安装Echarts，直接在终端输入如下命令:

```bash
npm install echarts@4.7.0 --save
```
> 注：指定版本号，因为最新版的ECharts不包含中国地图。

安装成功之后，我们会在package.json里看到如下echarts对应的版本号:
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426160607578.png)


### ECharts的使用

```html
<template>
  <div>
    <div id="chart" style="width: 600px;height:500px; margin: 0 auto">展示地图的地方</div>
  </div>
</template>

<script>
  import echarts from 'echarts'   //引入echarts
  import 'echarts/map/js/china'   //引入中国地图
  export default {
    name: "ECharts",
    data() {
      return {
        myChart:''
      }
    },
    mounted(){
      let option ={
        title:{   //标题
          text:'实时疫情地图',
          x:'center',   //居中
          textStyle:{   //标题 样式
            color:'#9c0505'
          }
        },
        tooltip:{  //提示信息
          trigger: 'item',   //类型
          //地图 : {a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
          formatter: '地区：{b}<br/>确诊：{c}'
        },
        series:[  //数据
          {
            type:'map',  //图表的类型
            map:'china',
            data:[
              {name: '北京', value: 100},
              {name: '湖北', value: 12000},
              {name: '湖南', value: 2000},
              {name: '西藏', value: 30},
            ],
            label:{  //图形上的文本标签，可用于说明图形的一些数据信息
              show:true,
              color:'red',
              fontSize:10
            },
            zoom:1.3,  //当前视角的缩放比例。
            itemStyle:{  //地图区域的多边形 图形样式。
              borderColor:'blue',
            },
            emphasis:{  //高亮状态下的设置
              label:{  //图形上的文本标签，可用于说明图形的一些数据信息
                color:'#fff',
                fontSize:12
              },
              itemStyle:{  //地图区域的多边形 图形样式。
                areaColor:'green',
              },
            }
          }
        ],
        visualMap: {  //视觉地图
          type:'piecewise',   //分段型
          show:true,
          pieces: [
            {min: 10000}, // 不指定 max，表示 max 为无限大（Infinity）。
            {min: 1000, max: 9999},
            {min: 100, max: 999},
            {min: 10, max: 99},
            {min: 1, max: 9},
            {value: 0}     // 不指定 min，表示 min 为无限大（-Infinity）。
          ],
          inRange: {  //范围
            color: ['#fff', '#ffaa85', '#660208'],
          },
          itemWidth:10,
          itemHeight:10
        },
        toolbox: {
          show: true,
          orient: 'horizontal',
          left: 'right',
          top: 'top',
          feature: {
            dataView: {readOnly: false},
            restore: {},
            saveAsImage: {}
          }
        },
      };
      // 基于准备好的dom，初始化echarts实例
      this.myChart = echarts.init(document.getElementById('chart'));
      // 使用刚指定的配置项和数据显示图表。
      this.myChart.setOption(option);
    }
  }
</script>

<style scoped>

</style>

```

效果图：
![ECharts效果图](./doc/image/echarts-china.png)
### 加载数据

#### 数据接口
一、新浪疫情数据接口：
http://interface.sina.cn/news/wap/fymap2020_data.d.json?_=1580892522427
二、腾讯的疫情数据，接口地址：
https://view.inews.qq.com/g2/getOnsInfo?name=disease_h5&callback=jQuery34102581268431257997_1582545445186&_=1582545445187

> 案例一：使用腾讯的疫情数据，案例网址：https://www.jianshu.com/p/293c4d7500eb?utm_campaign=hugo
 
三、开源项目疫情数据接口（数据来自丁香园）：
接口介绍网址：
https://lab.isaaclin.cn/nCoV/zh
https://github.com/BlankerL/DXY-COVID-19-Crawler
数据接口：https://lab.isaaclin.cn/nCoV/api/area

#### 方法一：jsonp实现跨域请求

> 注意：
> 跨域问题，简单理解：同一个ip、同一个网络协议、同一个端口，三者都满足就是同一个域，否则就是跨域问题了。为了系统的安全，所有支持JavaScript的浏览器遵循同源策略，即域名、协议、端口相同，属于同一个域，可以直接访问，浏览器默认不可跨域访问。
> jsonp虽然可以实现跨域请求，但是它只能用于get方法请求而不能用于post请求，另外，在实际项目开发中由于这种方式并不安全，在处理跨域问题时一般不用jsonp，现在大多数企业采用httpclient基于服务端的跨域解决技术。

在终端里用命令安装jsonp

```bash
npm install jsonp
```
引入jsonp
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426164627539.png)
请求数据

```html
<template>
  <div>
    <div id="chart" style="width: 600px;height:500px;">展示地图的地方</div>
  </div>
</template>

<script>
  import echarts from 'echarts'   //引入echarts
  import 'echarts/map/js/china'   //引入中国地图
  import jsonp from 'jsonp'       //引入jsonp
  let option ={
    title:{   //标题
      text:'实时疫情地图',
      x:'center',   //居中
      textStyle:{   //标题 样式
        color:'#9c0505'
      }
    },
    tooltip:{  //提示信息
      trigger: 'item',   //类型
      //地图 : {a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
      formatter: '地区：{b}<br/>确诊：{c}'
    },
    series:[  //数据
      {
        type:'map',  //图表的类型
        map:'china',
        data:[
          {name: '北京', value: 100},
          {name: '湖北', value: 12000},
          {name: '湖南', value: 2000},
          {name: '西藏', value: 30},
        ],
        label:{  //图形上的文本标签，可用于说明图形的一些数据信息
          show:true,
          color:'red',
          fontSize:10
        },
        zoom:1.3,  //当前视角的缩放比例。
        itemStyle:{  //地图区域的多边形 图形样式。
          borderColor:'blue',
        },
        emphasis:{  //高亮状态下的设置
          label:{  //图形上的文本标签，可用于说明图形的一些数据信息
            color:'#fff',
            fontSize:12
          },
          itemStyle:{  //地图区域的多边形 图形样式。
            areaColor:'green',
          },
        }
      }
    ],
    visualMap: {  //视觉地图
      type:'piecewise',   //分段型
      show:true,
      pieces: [
        {min: 10000}, // 不指定 max，表示 max 为无限大（Infinity）。
        {min: 1000, max: 9999},
        {min: 100, max: 999},
        {min: 10, max: 99},
        {min: 1, max: 9},
        {value: 0}     // 不指定 min，表示 min 为无限大（-Infinity）。
      ],
      inRange: {  //范围
        color: ['#fff', '#ffaa85', '#660208'],
      },
      itemWidth:10,
      itemHeight:10
    },
    toolbox: {
      show: true,
      orient: 'horizontal',
      left: 'right',
      top: 'top',
      feature: {
        dataView: {readOnly: false},
        restore: {},
        saveAsImage: {}
      }
    },
  };
  export default {
    name: "ECharts",
    data() {
      return {
        myChart:''
      }
    },
    mounted(){
      this.getData();
      // 基于准备好的dom，初始化echarts实例
      this.myChart = echarts.init(document.getElementById('chart'));
      // 使用刚指定的配置项和数据显示图表。
      this.myChart.setOption(option);
    },
    methods:{

      getData(){    //获取网络接口数据
        //jsonp('url',function(){})    //1585397547284
        jsonp('https://interface.sina.cn/news/wap/fymap2020_data.d.json?_=1585397547299',(err,data)=>{
          // console.log(data.data.list)
          //data.data.list
          //使用js里面map()方法，循环遍历后，获取数据里面一部分值。
          var lists = data.data.list.map(item=>{return {name: item.name, value: item.value}});
          console.log(lists);
          option.series[0].data = lists;
          // 使用刚指定的配置项和数据显示图表。
          this.myChart.setOption(option);
        })
      }
    }
  }
</script>

<style scoped>
  #chart {
    margin: 0 auto;
  }
</style>

```


#### 方法二：axios+devServer代理请求转发
配置devServer代理
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021042617091947.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)

```javascript

module.exports = {
  devServer: {
    /* 自动打开浏览器 */
    open: true,
    /* 设置为0.0.0.0则所有的地址均能访问 */
    host: '0.0.0.0',
    port: 8081,
    https: false,
    hotOnly: false,
    /* 使用代理 */
    proxy: {
      '/news/wap/fymap2020_data.d.json': {
        ws:false,
        /* 目标代理服务器地址 */
        target: 'https://interface.sina.cn',
        /* 允许跨域 */
        changeOrigin: true,
      },
    },
  },
}

```

在终端里用命令安装axios

```bash
npm install axios
```
引入axios
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426170819426.png)
请求数据

```html
<template>
  <div>
    <div id="chart" style="width: 600px;height:500px;">展示地图的地方</div>
  </div>
</template>

<script>
  import echarts from 'echarts'   //引入echarts
  import 'echarts/map/js/china'   //引入中国地图
  import axios from 'axios'       //引入axios
  let option ={
    title:{   //标题
      text:'实时疫情地图',
      x:'center',   //居中
      textStyle:{   //标题 样式
        color:'#9c0505'
      }
    },
    tooltip:{  //提示信息
      trigger: 'item',   //类型
      //地图 : {a}（系列名称），{b}（区域名称），{c}（合并数值）, {d}（无）
      formatter: '地区：{b}<br/>确诊：{c}'
    },
    series:[  //数据
      {
        type:'map',  //图表的类型
        map:'china',
        data:[
          {name: '北京', value: 100},
          {name: '湖北', value: 12000},
          {name: '湖南', value: 2000},
          {name: '西藏', value: 30},
        ],
        label:{  //图形上的文本标签，可用于说明图形的一些数据信息
          show:true,
          color:'red',
          fontSize:10
        },
        zoom:1.3,  //当前视角的缩放比例。
        itemStyle:{  //地图区域的多边形 图形样式。
          borderColor:'blue',
        },
        emphasis:{  //高亮状态下的设置
          label:{  //图形上的文本标签，可用于说明图形的一些数据信息
            color:'#fff',
            fontSize:12
          },
          itemStyle:{  //地图区域的多边形 图形样式。
            areaColor:'green',
          },
        }
      }
    ],
    visualMap: {  //视觉地图
      type:'piecewise',   //分段型
      show:true,
      pieces: [
        {min: 10000}, // 不指定 max，表示 max 为无限大（Infinity）。
        {min: 1000, max: 9999},
        {min: 100, max: 999},
        {min: 10, max: 99},
        {min: 1, max: 9},
        {value: 0}     // 不指定 min，表示 min 为无限大（-Infinity）。
      ],
      inRange: {  //范围
        color: ['#fff', '#ffaa85', '#660208'],
      },
      itemWidth:10,
      itemHeight:10
    },
    toolbox: {
      show: true,
      orient: 'horizontal',
      left: 'right',
      top: 'top',
      feature: {
        dataView: {readOnly: false},
        restore: {},
        saveAsImage: {}
      }
    },
  };
  export default {
    name: "ECharts",
    data() {
      return {
        myChart:''
      }
    },
    mounted(){
      this.getData();
      // 基于准备好的dom，初始化echarts实例
      this.myChart = echarts.init(document.getElementById('chart'));
      // 使用刚指定的配置项和数据显示图表。
      this.myChart.setOption(option);
    },
    methods:{
      getData(){    //获取网络接口数据
        axios.get('/news/wap/fymap2020_data.d.json?_=1585397547299')
        .then(data=>{
          data = data.data
          console.log(data.data.list)
          //data.data.list
          //使用js里面map()方法，循环遍历后，获取数据里面一部分值。
          var lists = data.data.list.map(item=>{return {name: item.name, value: item.value}});
          console.log(lists);
          option.series[0].data = lists;
          // 使用刚指定的配置项和数据显示图表。
          this.myChart.setOption(option);
        })
        .catch( (error) => {
          console.log(error);
        });
      }
    }
  }
</script>

<style scoped>
  #chart {
    margin: 0 auto;
  }
</style>

```
#### 文件比较
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021042617160116.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210426171708239.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI3Mjc4MQ==,size_16,color_FFFFFF,t_70)

#### 效果图
![截图展示](./doc/image/result.png)
# 心得体会
1. 掌握使用Vue-CLI脚手架工具在自己的电脑上建立项目，并运行调试工具。
2. 学习使用ECharts图表开源库开发地图展示功能，了解图表开发方法。

