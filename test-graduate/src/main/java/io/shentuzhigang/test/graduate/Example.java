package io.shentuzhigang.test.graduate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-13 19:37
 */
public class Example {


    // Mooctest Selenium Example
    public static void test(WebDriver driver) {
        driver.get("http://114.215.176.95:60511/Graduate/");
        driver.findElement(By.name("name")).sendKeys("adda");
        driver.findElement(By.name("pwd")).sendKeys("123");
        driver.findElement(By.cssSelector("label:nth-child(2)")).click();
        driver.findElement(By.cssSelector(".button")).click();
        driver.findElement(By.linkText("添加科室")).click();
        driver.switchTo().frame(0);
        driver.findElement(By.name("cNumber")).sendKeys(String.valueOf(System.currentTimeMillis()));
        driver.findElement(By.name("dName")).sendKeys("办公室");
        driver.findElement(By.name("dDec")).sendKeys("办公室");
        driver.switchTo().defaultContent();
        driver.findElement(By.linkText("科室信息")).click();
        driver.switchTo().frame(0);
        driver.findElement(By.id("keywords")).sendKeys("131");
        driver.findElement(By.linkText("搜索")).click();
        driver.findElement(By.linkText("修改")).click();
        driver.findElement(By.cssSelector(".button")).click();
        driver.switchTo().defaultContent();
        driver.findElement(By.cssSelector("h2:nth-child(4)")).click();
        // 等待css渲染
        WebDriverWait wait = new WebDriverWait(driver, 1);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("添加医生")));
        driver.findElement(By.linkText("添加医生")).click();
        driver.switchTo().frame(0);
        driver.findElement(By.name("dNumber")).sendKeys(String.valueOf(System.currentTimeMillis()));
        driver.findElement(By.name("name")).sendKeys("吴有海");
        driver.findElement(By.name("dPwd")).sendKeys("123456");
        {
            WebElement dropdown = driver.findElement(By.name("cNumber"));
            dropdown.findElement(By.xpath("//option[. = '耳鼻喉科']")).click();
        }
        driver.findElement(By.name("dInfo")).sendKeys("内分泌专家");
        driver.findElement(By.name("dResume")).sendKeys("主治医师");
        driver.findElement(By.name("dTel")).sendKeys("15300000000");
        driver.findElement(By.name("dEmail")).sendKeys("1600000000@qq.com");
        driver.findElement(By.cssSelector(".button")).click();
        driver.switchTo().defaultContent();
        driver.findElement(By.linkText("医生管理")).click();
        driver.switchTo().frame(0);
        {
            WebElement dropdown = driver.findElement(By.id("s_istop"));
            dropdown.findElement(By.xpath("//option[. = '医生编号']")).click();
        }
        driver.findElement(By.id("keywords")).sendKeys("1235");
        driver.findElement(By.linkText("搜索")).click();
        driver.findElement(By.linkText("修改")).click();
        driver.findElement(By.name("dInfo")).sendKeys("副主任医生");
        driver.findElement(By.cssSelector(".button")).click();
        driver.switchTo().defaultContent();
        driver.findElement(By.linkText("退出登录")).click();
    }


    // <!> Check if selenium-standalone.jar is added to build path.


    public static void main(String[] args) {
        // Run main function to test your script.
        ChromeOptions options = new ChromeOptions();
        options.setBinary(new File("D:/ext/Google/Chrome/Application/chrome.exe"));
        WebDriver driver = new ChromeDriver(options);
        try {
            test(driver);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }


}
