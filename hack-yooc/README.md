# 易班优课YOOC课群在线测试自动答题解决方案

## 基本概念
易班优课YOOC：优课YOOC”是易班网于2016年4月研发并推出的基于Social Learning的理念而开发的在线学习平台。

官方网站：https://www.yooc.me/

## 解决方案
### 基本方案
[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(一)答案获取](https://shentuzhigang.blog.csdn.net/article/details/105758319)

[Spring Boot——易班优课YOOC课群在线测试自动答题解决方案(二)答案储存]()

[Spring Boot——易班优课YOOC课群在线测试自动答题解决方案(三)答案查询]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(四)答案显示]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(五)简单脚本]()

### 扩展方案
[Spring Boot——易班优课YOOC课群在线测试自动答题解决方案(六)后端改造]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(七)随机答案]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(八)功能面板]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(九)ID标签]()

[Vue + Element UI + Spring Boot——易班优课YOOC课群在线测试自动答题解决方案(十)问题管理页面]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十一)恢复右键、选择和复制]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十二)脚本整合]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十九)强制重做]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(二十一)禁止打开控制台解决方案]()

### 自动方案
[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十三)自动答题]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十四)自动刷题]()

### 升级方案
[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十五)整合升级+引入jQuery]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(十六)利用PC端和移动端BUG]()

[JavaScript + Tampermonkey——易班优课YOOC课群在线测试自动答题解决方案(十七)复合型解决方案油猴脚本]()

### 管理方案
[JavaScript + Thymeleaf + Spring Boot——易班优课YOOC课群在线测试自动答题解决方案(十八)模板脚本]()

### 维护方案
[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(二十)整理维护]()

[JavaScript——易班优课YOOC课群在线测试自动答题解决方案(二十二)脚本更新3.1](https://shentuzhigang.blog.csdn.net/article/details/108351690)

## 参考文章
https://www.52pojie.cn/thread-966870-1-1.html

https://download.csdn.net/download/qq_41011723/10763113

https://blog.csdn.net/weixin_43597018/article/details/83713247

https://gitee.com/pldq/ExamAutoSolution

https://blog.csdn.net/sinat_26935081/article/details/79172715

https://blog.csdn.net/zzh_97580460/article/details/99934372

https://blog.csdn.net/weixin_43272781/article/details/105774421

http://caibaojian.com/javascript/107.html

https://blog.csdn.net/zzti_erlie/article/details/89842391

https://www.runoob.com/regexp/regexp-syntax.html

https://www.cnblogs.com/ChengDong/articles/3771880.html

https://zhuanlan.zhihu.com/p/33335629

https://www.cnblogs.com/tangyongle/p/7499522.html

https://zhidao.baidu.com/question/495418166779664644.html

https://jingyan.baidu.com/article/0320e2c1bd21b25b86507b0d.html