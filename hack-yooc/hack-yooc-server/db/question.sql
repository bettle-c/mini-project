/*
 Navicat Premium Data Transfer

 Source Server         : RDS MySQL
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : rm-bp17m6mdi0y0ne9g2co.mysql.rds.aliyuncs.com:3306
 Source Schema         : stzg

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 07/06/2021 00:37:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question_exam_yooc`;
CREATE TABLE question_exam_yooc  (
     `id` varchar(255) NOT NULL,
     `group` varchar(255) NULL,
     `exam` varchar(255) NULL,
     `question` varchar(8192) NULL,
     PRIMARY KEY (`id`)
);
SET FOREIGN_KEY_CHECKS = 1;
