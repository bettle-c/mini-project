package io.shentuzhigang.hack.yooc.dto;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-10 12:51
 */
public class ApiResponse {
    private String code;

    private String msg;

    private Object data;

    @Override
    public String toString() {
        return "ApiResponse{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return this.msg;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return this.code;
    }
    public void setData(Object data) {
        this.data = data;
    }
    public Object getData() {
        return this.data;
    }
}