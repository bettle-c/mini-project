package io.shentuzhigang.hack.yooc.utils;

import io.shentuzhigang.hack.yooc.dto.ApiResponse;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-11 17:48
 */
public class ApiResponseUtil {
    //正确请求响应内容
    public static ApiResponse getRetTemp(){
        ApiResponse ret = new ApiResponse();
        ret.setCode("200");
        ret.setMsg("success");
        ret.setData(null);
        return ret;
    }
}
