package io.shentuzhigang.hack.yooc.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 09:42
 */
@TableName(value = "question_exam_yooc")
public class Question {
    @TableField("`group`")
    private String group;

    private String exam;
    @TableId(value="id")
    private String id;

    private String question;
    public Question(){

    }
    public Question(String group, String exam) {
        this.group = group;
        this.exam = exam;
    }
    public Question(String group, String exam, String id, String question) {
        this.group = group;
        this.exam = exam;
        this.id = id;
        this.question = question;
    }

    @Override
    public String toString() {
        return "QuestionDTO{" +
                "group='" + group + '\'' +
                ", exam='" + exam + '\'' +
                ", id='" + id + '\'' +
                ", question='" + question + '\'' +
                '}';
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

}
