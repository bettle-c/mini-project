package io.shentuzhigang.hack.yooc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-07 00:28
 */
@SpringBootApplication
public class YoocApplication {
    public static void main(String[] args) {
        SpringApplication.run(YoocApplication.class, args);
    }

}

