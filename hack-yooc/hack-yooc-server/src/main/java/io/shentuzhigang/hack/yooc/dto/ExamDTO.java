package io.shentuzhigang.hack.yooc.dto;

import java.util.List;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 08:29
 */

public class ExamDTO {
    private String group;
    private String exam;
    private List<QuestionDTO> questions;

    @Override
    public String toString() {
        return "ExamDTO{" +
                "group='" + group + '\'' +
                ", exam='" + exam + '\'' +
                ", questions=" + questions +
                '}';
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }
}
