package io.shentuzhigang.hack.yooc.service;

import io.shentuzhigang.hack.yooc.model.Question;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-04-26 09:39
 */
public interface IYOOCExamQuestionService extends IService<Question> {
}
