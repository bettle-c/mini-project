package io.shentuzhigang.crawler.cet.service.impl;


import cn.hutool.http.HttpRequest;
import io.shentuzhigang.crawler.cet.service.ICETService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-21 11:31
 */

@Service
public class CETServiceImpl implements ICETService {
    private static final String QUERY_URL = "http://cache.neea.edu.cn/cet/query";
    private static final Pattern PATTERN = Pattern.compile("result.callback\\((.*)\\);");
    @Override
    public String query(String zkzh, String name){
        Map<String,String> headers=new HashMap<>();
        headers.put("Referer","http://cet.neea.edu.cn/cet/query.html");
        char idx = zkzh.charAt(9);
        String z = null;
        if(idx == '1'){
            z = "CET4_192_DANGCI";
        }else if(idx == '2'){
            z = "CET6_192_DANGCI";
        }
        String url = QUERY_URL + "?data="+z + "," + zkzh + "," + name;
        HttpRequest request =  HttpRequest.get(QUERY_URL);
        request.addHeaders(headers);
        String res = request.execute().body();
        // String res = HttpClientUtils.doGetRequest(QUERY_URL,headers,params);
        Matcher m = PATTERN.matcher(res);
        String obj = "";
        if (m.find()) {
            obj = m.group(1);
        }
        return obj;
    }
}
