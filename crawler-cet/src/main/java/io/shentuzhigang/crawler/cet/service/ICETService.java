package io.shentuzhigang.crawler.cet.service;

/**
 * @Author ShenTuZhiGang
 * @Version 1.0.0
 * @Date 2020-02-22 10:05
 */
public interface ICETService {
    String query(String zkzh, String name);
}
