#!usr/bin/env python
# -*- coding:utf-8 _*-
"""
@version: 0.0.1
@author: ShenTuZhiGang
@time: 2021/01/29 10:46
@file: paddleocrdemo.py
@function:
@last modified by: ShenTuZhiGang
@last modified time: 2021/01/29 10:46
"""
import cv2
import paddleocr
import paddle
from PIL import Image, ImageDraw, ImageFont
import numpy as np
res = paddleocr.PaddleOCR(gpu_mem=1000).text_recognizer.ocr(img=r'D:\Code\Project\PaddleOCR\data\text_detection\test\image\img_1.png')

img = Image.open(r'D:\Code\Project\PaddleOCR\data\text_detection\test\image\img_1.png')
im = np.array(img)
for i in range(len(res)):
    print(res[i])
    print(res[i][0])
    print(res[i][0][3][0])
    print(res[i][0][3][1])
    print(res[i][1])
    # data[d['text'][i]] = ([d['left'][i], d['top'][i], d['width'][i], d['height'][i]])
    cv2.rectangle(im, (int(res[i][0][0][0]), int(res[i][0][0][1])),
                  (int(res[i][0][2][0]), int(res[i][0][2][1])), (255, 0, 0), 1)
    # 使用cv2.putText不能显示中文，需要使用下面的代码代替
    # cv2.putText(im, d['text'][i], (x, y-8), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)

    pilimg = Image.fromarray(im)
    pilimg.resize((400, 300),Image.ANTIALIAS)
    draw = ImageDraw.Draw(pilimg)
    # 参数1：字体文件路径，参数2：字体大小
    font = ImageFont.truetype("simhei.ttf", 15, encoding="utf-8")
    # 参数1：打印坐标，参数2：文本，参数3：字体颜色，参数4：字体
    text, sroce = res[i][1]
    draw.text((res[i][0][0][0], res[i][0][0][1]), text, (255, 0, 0), font=font)
    im = cv2.cvtColor(np.array(pilimg), cv2.COLOR_RGB2BGR)

cv2.imshow("recoText", im)
cv2.waitKey(0)
cv2.destroyAllWindows()


