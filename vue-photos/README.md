> **代码仓库**：[https://gitee.com/shentuzhigang/demo-project/tree/master/vue-photos](https://gitee.com/shentuzhigang/demo-project/tree/master/vue-photos) 
> 
> **Blog**：[https://shentuzhigang.blog.csdn.net/article/details/116598452](https://shentuzhigang.blog.csdn.net/article/details/116598452)
# 实验目标
1. 掌握使用Vue-CLI脚手架工具在自己的电脑上建立项目，并会运行调试工具。
2. 理解组件化开发思想。
3. 图片轮播手机网页。
---
# 实验内容
1. 要求使用Vue-CLI脚手架工具搭建一个Web项目vue-photo（本次实验必须用Vue-CLI脚手架搭建项目）。实验报告要求将项目文件结构截图，并简单介绍。
2. 参照源码效果，实现一个图片轮播预览的手机网页。使用Vue组件编程方法完成主要功能，具体使用的编程技术不限。
3. 功能上要求实现最基本的指定图片浏览功能。
4. 自选扩展实验：模仿手机上的相机图片预览功能，实现手机内图片预览。本条内容根据自己的学习情况，可选做。
---





## 截图展示



## 主要代码及实现方法简介



# 心得体会



