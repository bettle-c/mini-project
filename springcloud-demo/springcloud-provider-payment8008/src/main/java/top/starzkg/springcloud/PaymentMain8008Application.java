package top.starzkg.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 01:21
 */
@SpringBootApplication
@EnableDiscoveryClient
public class PaymentMain8008Application {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8008Application.class,args);
    }
}
