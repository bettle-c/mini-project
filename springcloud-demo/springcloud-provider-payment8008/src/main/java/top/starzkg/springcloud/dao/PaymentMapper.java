package top.starzkg.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import top.starzkg.springcloud.entity.Payment;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 12:52
 */
@Mapper
@Component
public interface PaymentMapper {

    int create(Payment payment);
    Payment getPaymentById(@Param("id")Long id);
}
