package top.starzkg.springcloud.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.starzkg.springcloud.dao.PaymentMapper;
import top.starzkg.springcloud.entity.Payment;
import top.starzkg.springcloud.service.IPaymentService;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 13:09
 */
@Service
public class PaymentServiceImpl implements IPaymentService {
    @Autowired
    private PaymentMapper paymentMapper;
    @Override
    public int create(Payment payment) {
        return paymentMapper.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentMapper.getPaymentById(id);
    }
}
