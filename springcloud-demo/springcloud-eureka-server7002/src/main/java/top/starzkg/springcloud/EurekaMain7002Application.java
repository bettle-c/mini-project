package top.starzkg.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2020-11-15 16:50
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaMain7002Application {
    public static void main(String[] args) {
        SpringApplication.run(EurekaMain7002Application.class,args);
    }
}
