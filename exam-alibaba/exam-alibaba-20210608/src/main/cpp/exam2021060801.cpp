#include <iostream>
using namespace std;

long pow(int n,int p){
    long res= 1;
    for(int i=1;i<=p;i++){
        res=res*n;
    }
    return res;
}
int func(int n,long k){
    long mid = pow(2,n-1);
    if(mid==k){
        return n;
    }else if(k>mid){
        return func(n-1,k-mid);
    }else{
        return func(n-1,k);
    }
}

int main(){
    int n;
    long k;
    cin>>n>>k;
    cout<<func(n,k)<<endl;
}


