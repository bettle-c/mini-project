package io.shentuzhigang.exam.alibaba;

import java.util.Scanner;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @email 1600337300@qq.com
 * @date 2021-06-09 11:46
 */
public class Exam2021060801 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        long k = in.nextLong();
        System.out.println(func(n, k));
    }

    public static int func(int n, long k) {
        long mid = (long) Math.pow(2, n - 1);
        if (mid == k) {
            return n;
        } else if (k > mid) {
            return func(n - 1, k - mid);
        } else {
            return func(n - 1, k);
        }
    }
}
