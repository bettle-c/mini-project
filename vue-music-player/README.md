[English](README_en.md) | 简体中文

# vue-music-player

> **代码仓库**: https://gitee.com/shentuzhigang/demo-project/tree/master/vue-music-player
>
> **Blog**: https://shentuzhigang.blog.csdn.net/article/details/116936161


# 实验目标
1. 掌握使用Vue-CLI脚手架工具在自己的电脑上建立项目，并会运行调试工具。
2. 了解vue-aplayer插件的使用方法。
3. 理解如何使用 axios 发起 http 请求的方法。
4. 使用QQ音乐、网易云音乐等API接口开发简单音乐播放手机应用，学习通过网络接口的调用，培养复杂问题简单化思维。
# 实验内容
1. 要求使用Vue-CLI脚手架工具搭建一个Web项目vue-music（本次实验必须用Vue-CLI脚手架搭建项目）。实验报告要求将项目文件结构截图，并简单介绍。
2. 安装依赖，使用Node.js运行mock-serve中server服务，运行Music-play-front中源码，参照运行效果，实现一个简单手机音乐播放应用网页。
3. 使用Vue组件编程方法完成主要功能，具体使用的编程技术不限。
4. 实现最基本的音乐播放功能即可完成实验基础内容（基础部分满分96分）。
5. 自选扩展实验：模仿手机上的QQ音乐播放器，实现一个手机音乐播放器Web应用。本条扩展内容请根据自己的学习情况选做（4分）。

---
## 截图展示
![](docs/images/3.png)
![](docs/images/4.png)


## 主要代码及实现方法简介（请尽量配合截图，描述清楚编程和开发过程和方法）
```html
<div class="playBox" v-if="songDATA">
    <div class="bg">
      <img :src="songDATA.pic" alt/>
    </div>

    <div class="content">
      <div class="header">
        <h2 v-text="songDATA.title"></h2>
        <p v-text="songDATA.author"></p>
      </div>
      <div class="lyric swiper-container" ref="lyric">
        <div class="wrapper swiper-wrapper">
          <p
              v-for="(item,index) in songDATA.lyric"
              v-text="item.content"
              :key="index"
              :class="`swiper-slide ${lyricIndex===index?'active':''}`"
          ></p>
        </div>
      </div>
      <div class="handle">
        <i @click="openVideo(songDATA.id)"></i>
        <i ref="audioBtn"></i>
        <i></i>
      </div>
    </div>

    <audio :src="songDATA.music" style="display:none" ref="audio"></audio>

    <!-- 热门推荐 -->
    <recommend></recommend>
  </div>
```
```javascript
audioHandle() {
  // 控制音乐的自动播放
  let play = () => {
    this.audioPlay();
    document.removeEventListener("touchstart", play);
  };
  play();
  document.addEventListener("WeixinJSBridgeReady", play);
  document.addEventListener("touchstart", play);

  // 点击按钮控制暂停或者继续播放
  let {audio, audioBtn} = this.$refs;
  if (!audio) return;
  audioBtn.onclick = () => {
    if (audio.paused) {
      this.audioPlay();
      return;
    }
    this.audioPause();
  };
},
lyricAuto() {
  let {audio} = this.$refs;
  if (!audio) return;
  window.clearInterval(this.lyricTimer);
  this.lyricTimer = window.setInterval(() => {
    let duration = audio.duration,
        currentTime = audio.currentTime;
    if (currentTime >= duration) {
      this.audioComplete();
      return;
    }
    // 歌词对应和跟着动
    if (!this.songDATA) return;
    let n;
    this.songDATA.lyric.find((item, index) => {
      let {minutes, seconds} = item;
      if (minutes * 60 + parseInt(seconds) === Math.round(currentTime)) {
        n = index;
        return true;
      }
      return false;
    });
    if (n && n !== this.lyricIndex) {
      this.lyricIndex = n;
      let index = this.lyricIndex - 3;
      index < 0 ? (index = 0) : null;
      this.$swiper.slideTo(index, 200);
    }
  }, 1000);
}
```
```vue
<template>
  <div>
    <div style="padding:10px 0;">
      <a-player
          v-model:music="currentMusic"
          :list="songList"
          :showlrc="3"
          :narrow="false"
          theme="#b7daff"
          mode="circulation"
          v-if="flag"
          listmaxheight='96px'
          ref="player"></a-player>
    </div>
  </div>
</template>

<script>
  import axios from 'axios'
  // Vue3 不支持vue-router
  import VueAplayer from 'vue3-aplayer'
  export default {
    name: "NeteaseCloudMusicPlayer",
    components: {
      'a-player': VueAplayer
    },
    data() {
      return {
        flag:false,
        currentMusic: {},
        songList:[]
      }
    },
    async mounted () {
      //异步加载，先加载出player再使用
      await this.init();
      let aplayer = this.$refs.player;
      this.currentMusic = this.songList[0]
      aplayer.play();
    },
    methods:{
      async init () {
        //这边是引入了axios然后使用的get请求的一个音乐列表接口
        //这边url随大家更改了
        let data = await axios.get("http://localhost:3000/playlist/detail?id=24381616");  //使用await ，data会等到异步请求的结果回来后才进行赋值
        //以下就是这边对请求的一个处理，看接口了
        if(data && data.code===200){       //200: '服务器成功返回请求的数据
          let getMusicList=data.playlist.tracks;
          for(let i=0;i<getMusicList.length;i++){
            let obj={};
            obj.title = getMusicList[i].name;
            obj.artist =  getMusicList[i].ar[0].name;
            obj.pic =  getMusicList[i].al.picUrl;
            obj.id = getMusicList[i].id;
            let url =await axios.get("http://localhost:3000/song/url?id="+obj.id);
            obj.src =url.data[0].url
            //把数据一个个push到songList数组中，在a-player标签中使用 :music="songList" 就OK了
            this.songList.push(obj);
          }
          //因为是异步请求，所以一开始播放器中是没有歌曲的，所有给了个v-if不然会插件默认会先生成播放器，导致报错(这个很重要)
          this.flag = true;
        }
      }
    }
  }
</script>
```

## 运行说明
### mock
mock 部分程序是后台Web服务，这里使用Express 框架创建后台服务。
> Express 是一个简洁而灵活的 node.js Web应用框架, 提供一系列强大特性帮助你创建各种Web应用。

第一步：
1.	使用cmd打开控制台；
2.	进入mock文件夹，使用yarn install 安装依赖；
3.	输入node serve.js运行后台服务。
![](docs/images/1.png)

第二步：
1.	使用cmd打开控制台；
2.	进入根文件夹，使用yarn install 安装依赖；
3.	输入yarn run或者yarn serve运行代码（或者IDE）；
![](docs/images/2.png)

第三步： 
1. 通过手机或电脑浏览器访问网站。
   http://localhost:8080/MusicPlayer/1

### NeteaseCloudMusicApi

1. 安装NeteaseCloudMusicApi

```shell
$ git clone git@github.com:Binaryify/NeteaseCloudMusicApi.git 
$ npm install
```

或者
```shell
$ git clone https://github.com/Binaryify/NeteaseCloudMusicApi.git
$ npm install
```
> 手动下载:<br>
> 下载网易云音乐 NodeJS 版 API GitHub：https://github.com/Binaryify/NeteaseCloudMusicApi
2. 运行NeteaseCloudMusicApi

```shell
$ node app.js
```

服务器启动默认端口为 3000,若不想使用 3000 端口,可使用以下命令: Mac/Linux

```shell
$ PORT=4000 node app.js
```

windows 下使用 git-bash 或者 cmder 等终端执行以下命令:

```shell
$ set PORT=4000 && node app.js
```
3. 访问
   http://localhost:8080/NeteaseCloudMusicPlayer


# 心得体会
1. 掌握使用Vue-CLI脚手架工具在自己的电脑上建立项目，并运行调试工具。
2. 学习Vue组件编程方法。
3. 实现最基本的音乐播放功能。
4. 搭建Mock。
5. 了解vue-aplayer插件的使用方法。
6. 理解如何使用 axios 发起 http 请求的方法。

# 常见问题
- [Vue Swiper 找不到swiper.css 报错This dependency was not found:swiper/dist/css/swiper.css](https://blog.csdn.net/A_linyuan/article/details/108005713)
- [vue中vuex添加Logger插件](https://blog.csdn.net/qq_43363884/article/details/103783669)

# 参考文档
- [NeteaseCloudMusicApi](https://neteasecloudmusicapi.vercel.app/)
- [Vue APlayer](https://github.com/SevenOutman/vue-aplayer/blob/develop/docs/README.zh-CN.md)

# 参考文章
- [vue-aplayer 音乐播放，实现播放与音乐列表](https://blog.csdn.net/qq_41216743/article/details/106602863)
- [在vue中使用NeteaseCloudMusicApi并调用这个接口获取数据](https://blog.csdn.net/m0_45309753/article/details/109407594)
