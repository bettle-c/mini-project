import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import api from '@/api';

let app = createApp(App)
// https://vue3js.cn/docs/zh/guide/migration/global-api.html#config-productiontip-移除
// app.config.productionTip = false
// https://vue3js.cn/docs/zh/api/application-config.html#globalproperties
app.config.globalProperties.$api = api;

app.use(store)
  .use(router)
  .mount('#app')
