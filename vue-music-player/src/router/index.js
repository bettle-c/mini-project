import {createRouter, createWebHistory} from 'vue-router'
import MusicPlayer from "@/views/MusicPlayer";
import VideoPlayer from "@/views/VideoPlayer";
import NeteaseCloudMusicPlayer from "@/views/NeteaseCloudMusicPlayer";

const routes = [
  {
    path: '/',
    redirect: '/MusicPlayer/1'
  }, {
    path: '/MusicPlayer/:id',
    name: 'MusicPlayer',
    component: MusicPlayer
  }, {
    path: '/VideoPlayer/:id',
    name: 'VideoPlayer',
    component: VideoPlayer
  }, {
    path: '/NeteaseCloudMusicPlayer',
    name: 'NeteaseCloudMusicPlayer',
    component: NeteaseCloudMusicPlayer
  }, {
    path: '/*',
    redirect: '/MusicPlayer/1'
  }
]

const router = createRouter({
  mode: "hash",
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
