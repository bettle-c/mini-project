package io.shentuzhigang.test.nextday;

public abstract class CalendarUnit {
    protected int currentPos;

    protected int getCurrentPos() {
        return currentPos;
    }

    protected void setCurrentPos(int pCurrentPos) {
        currentPos = pCurrentPos;
    }

    protected abstract boolean increment();

    protected abstract boolean isValid();
}
