package io.shentuzhigang.test.blackbox;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author ShenTuZhiGang
 * @version 1.0.0
 * @date 2021-05-13 14:55
 */
@RunWith(Parameterized.class)
public class DateBoundaryTests {
    private int input1;
    private int input2;
    private int input3;
    private String expected;

    @Parameterized.Parameters
    public static Collection<?> prepareData(){
        Object [][] object = {
                //年
                {1889, 7, 10, "年的值不在指定范围内"},
                {1900, 7, 10, "19000711"},
                {1901, 7, 10, "19010711"},
                {2049, 7, 10, "20490711"},
                {2050, 7, 10, "20500711"},
                {2051, 7, 10, "年的值不在指定范围内"},
                //月
                {2019, 0, 10, "月的值不在指定范围内"},
                {2019, 1, 10, "20190111"},
                {2019, 2, 10, "20190211"},
                {2019, 11, 10, "20191111"},
                {2019, 12, 10, "20191211"},
                {2019, 13, 10, "月的值不在指定范围内"},
                //日
                {2019, 2, 27, "20190228"},
                {2019, 2, 28, "20190301"},
                {2019, 2, 29, "日的值不在指定范围内"},
                {2020, 2, 28, "20200229"},
                {2020, 2, 29, "20200301"},
                {2020, 2, 30, "日的值不在指定范围内"},

                {2019, 1, 30, "20190131"},
                {2019, 1, 31, "20190201"},
                {2019, 1, 32, "日的值不在指定范围内"},
                {2019, 4, 30, "20190501"},
                {2019, 4, 31, "日的值不在指定范围内"},
                {2019, 4, 32, "日的值不在指定范围内"},
                
        };
        return Arrays.asList(object);
    }
    public DateBoundaryTests(int input1,int input2,int input3,String expected){
        this.input1 = input1;
        this.input2 = input2;
        this.input3 = input3;
        this.expected = expected;

    }
    @Test
    public void testDate(){
        String result = Exp2.nextDate(input1,input2,input3);
        Assert.assertEquals(expected, result);
    }
}
