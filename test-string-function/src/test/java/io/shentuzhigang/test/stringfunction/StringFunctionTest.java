package io.shentuzhigang.test.stringfunction;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringFunctionTest {

    @Test(timeout = 4000)
    public void test() {
        // 判断是不是回文字符串
        assertFalse(StringFunction.isPalindromeInPlace("abc"));
        assertTrue(StringFunction.isPalindromeInPlace("aba"));

        // 获取子字符串集合
        assertArrayEquals(new String[]{"", "2", "1", "21" },
                StringFunction.generateSubsets("12"));

        // 莱文斯坦距离
        assertEquals(2,
                StringFunction.levenshteinDistanceIterative("abc", "a"));
        assertEquals(2,
                StringFunction.levenshteinDistanceRecursive("abc", "a"));

        // 翻转字符串
        assertEquals("",
                StringFunction.reverseWordsInPlace(""));

        assertEquals("abc123",
                StringFunction.reverseWordsInPlace("abc123"));

        assertEquals("1234  abc",
                StringFunction.reverseWordsInPlace("abc  1234"));

        assertEquals("efg 123  abcd",
                StringFunction.reverseWordsInPlace("abcd  123 efg"));

        // 翻转字符串
        assertEquals("123  abcd",
                StringFunction.reverseWordsByCharWithAdditionalStorage("abcd  123"));

        assertEquals("efg 123  abcd",
                StringFunction.reverseWordsByCharWithAdditionalStorage("abcd  123 efg"));

    }

    /**
     * 获取最长回文子序列
     */
    @Test(timeout = 4000)
    public void test1() {
        assertNull(Manacher.getLongestPalindromicSubstring(null));
        assertEquals("",
                Manacher.getLongestPalindromicSubstring(""));
        assertEquals("a",
                Manacher.getLongestPalindromicSubstring("abc123"));
        assertEquals("a",
                Manacher.getLongestPalindromicSubstring("abc 123"));
        assertEquals("a",
                Manacher.getLongestPalindromicSubstring("abc1234"));
        assertEquals("1221",
                Manacher.getLongestPalindromicSubstring("aba1221"));
    }
}
