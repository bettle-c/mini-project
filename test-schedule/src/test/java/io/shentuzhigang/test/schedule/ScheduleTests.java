package io.shentuzhigang.test.schedule;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ScheduleTests {

    @Test
    public void testFCFS() {
        Schedule s = new Schedule();
        List<Work> l = new ArrayList<>();
        l.add(new Work("P1", 0, 7));
        l.add(new Work("P2", 2, 4));
        l.add(new Work("P3", 4, 1));
        l.add(new Work("P4", 5, 4));
        List<Double> outcome = s.FCFS(l);
        assertEquals(8.75, outcome.get(0), 0.001);
        assertEquals(3.5, outcome.get(1), 0.001);

    }

    @Test
    public void testSJF01() {
        Schedule s = new Schedule();
        List<Work> l = new ArrayList<>();
        l.add(new Work("P1", 1, 7));
        l.add(new Work("P2", 2, 4));
        l.add(new Work("P3", 4, 1));
        l.add(new Work("P4", 5, 4));
        l.add(new Work("P5", 1, 5));
        l.add(new Work("P6", 2, 4));
        l.add(new Work("P7", 4, 1));
        l.add(new Work("P8", 5, 6));
        l.add(new Work("P9", 3, 3));
        List<Double> outcome = s.SJF(l);
        assertEquals(14.111, outcome.get(0), 0.001);
        assertEquals(3.518, outcome.get(1), 0.001);

    }

    @Test
    public void testWork() {
        assertEquals(0, new Work("P1", 0, 5).getPriority());
    }

}
